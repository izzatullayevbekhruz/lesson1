package com.example.lesson_1.controller;

import com.example.lesson_1.dto.ApiResponse;
import com.example.lesson_1.dto.WorkerDto;
import com.example.lesson_1.repository.WorkerRepository;
import com.example.lesson_1.service.WorkerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api/worker")
@RestController
@RequiredArgsConstructor
public class WorkerController {
    final WorkerRepository workerRepository;
    final WorkerService workerService;

    @GetMapping
    public HttpEntity<?> getAll(){
        return ResponseEntity.ok().body(workerRepository.findAll());
    }
    @PostMapping
    public HttpEntity<?> add(@Valid @RequestBody WorkerDto dto){
        ApiResponse apiResponse=workerService.add(dto);
        return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }
    @PutMapping("/{id}")
    public HttpEntity<?> edit(@PathVariable Integer id,@Valid @RequestBody WorkerDto dto){
        ApiResponse apiResponse=workerService.edit(id,dto);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }
    @DeleteMapping("/{id}")
    public HttpEntity<?> delete(@PathVariable Integer id){
        ApiResponse apiResponse=workerService.delete(id);
        return ResponseEntity.status(apiResponse.isSuccess()?204:404).body(apiResponse);
    }
}
