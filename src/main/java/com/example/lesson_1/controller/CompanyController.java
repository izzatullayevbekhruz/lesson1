package com.example.lesson_1.controller;

import com.example.lesson_1.dto.ApiResponse;
import com.example.lesson_1.dto.CompanyDto;
import com.example.lesson_1.repository.CompanyRepository;
import com.example.lesson_1.service.CompanyService;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.PermitAll;
import javax.persistence.Column;
import javax.validation.Valid;

@RequestMapping("/api/company")
@RestController
@RequiredArgsConstructor
public class CompanyController {
    final CompanyRepository companyRepository;
    final CompanyService companyService;

    @GetMapping
    public HttpEntity<?> getAll(){
        return ResponseEntity.ok().body(companyRepository.findAll());
    }
    @PostMapping
    public HttpEntity<?> add(@Valid @RequestBody CompanyDto dto){
        ApiResponse apiResponse=companyService.add(dto);
        return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }
    @PutMapping("/{id}")
    public HttpEntity<?> edit(@PathVariable Integer id,@Valid @RequestBody CompanyDto dto){
        ApiResponse apiResponse=companyService.edit(id,dto);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }
    @DeleteMapping("/{id}")
    public HttpEntity<?> delete(@PathVariable Integer id){
        ApiResponse apiResponse=companyService.delete(id);
        return ResponseEntity.status(apiResponse.isSuccess()?204:404).body(apiResponse);
    }
}
